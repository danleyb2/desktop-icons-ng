# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-09 21:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr ""

#: askRenamePopup.js:42
msgid "File name"
msgstr ""

#: askRenamePopup.js:49 desktopManager.js:811
msgid "OK"
msgstr ""

#: askRenamePopup.js:49
msgid "Rename"
msgstr ""

#: desktopIconsUtil.js:88
msgid "Command not found"
msgstr ""

#: desktopIconsUtil.js:225
msgid "Do you want to run “{0}”, or display its contents?"
msgstr ""

#: desktopIconsUtil.js:226
msgid "“{0}” is an executable text file."
msgstr ""

#: desktopIconsUtil.js:230
msgid "Execute in a terminal"
msgstr ""

#: desktopIconsUtil.js:232
msgid "Show"
msgstr ""

#: desktopIconsUtil.js:234 desktopManager.js:813 fileItemMenu.js:338
msgid "Cancel"
msgstr ""

#: desktopIconsUtil.js:236
msgid "Execute"
msgstr ""

#: desktopManager.js:199
msgid "Nautilus File Manager not found"
msgstr ""

#: desktopManager.js:200
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: desktopManager.js:774
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:815
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:875 desktopManager.js:1520
msgid "New Folder"
msgstr ""

#: desktopManager.js:879
msgid "New Document"
msgstr ""

#: desktopManager.js:884
msgid "Paste"
msgstr ""

#: desktopManager.js:888
msgid "Undo"
msgstr ""

#: desktopManager.js:892
msgid "Redo"
msgstr ""

#: desktopManager.js:898
msgid "Select All"
msgstr ""

#: desktopManager.js:906
msgid "Show Desktop in Files"
msgstr ""

#: desktopManager.js:910 fileItemMenu.js:267
msgid "Open in Terminal"
msgstr ""

#: desktopManager.js:916
msgid "Change Background…"
msgstr ""

#: desktopManager.js:925
msgid "Display Settings"
msgstr ""

#: desktopManager.js:932
msgid "Desktop Icons Settings"
msgstr ""

#: desktopManager.js:1578
msgid "Arrange Icons"
msgstr ""

#: desktopManager.js:1582
msgid "Arrange By..."
msgstr ""

#: desktopManager.js:1591
msgid "Keep Arranged..."
msgstr ""

#: desktopManager.js:1595
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1600
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: desktopManager.js:1606
msgid "Sort by Name"
msgstr ""

#: desktopManager.js:1608
msgid "Sort by Name Descending"
msgstr ""

#: desktopManager.js:1611
msgid "Sort by Modified Time"
msgstr ""

#: desktopManager.js:1614
msgid "Sort by Type"
msgstr ""

#: desktopManager.js:1617
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:136
msgid "Home"
msgstr ""

#: fileItemMenu.js:105
msgid "Open All..."
msgstr ""

#: fileItemMenu.js:105
msgid "Open"
msgstr ""

#: fileItemMenu.js:116
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:116
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:128
msgid "Scripts"
msgstr ""

#: fileItemMenu.js:134
msgid "Open All With Other Application..."
msgstr ""

#: fileItemMenu.js:134
msgid "Open With Other Application"
msgstr ""

#: fileItemMenu.js:140
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItemMenu.js:150
msgid "Cut"
msgstr ""

#: fileItemMenu.js:155
msgid "Copy"
msgstr ""

#: fileItemMenu.js:161
msgid "Rename…"
msgstr ""

#: fileItemMenu.js:169
msgid "Move to Trash"
msgstr ""

#: fileItemMenu.js:175
msgid "Delete permanently"
msgstr ""

#: fileItemMenu.js:183
msgid "Don't Allow Launching"
msgstr ""

#: fileItemMenu.js:183
msgid "Allow Launching"
msgstr ""

#: fileItemMenu.js:194
msgid "Empty Trash"
msgstr ""

#: fileItemMenu.js:205
msgid "Eject"
msgstr ""

#: fileItemMenu.js:211
msgid "Unmount"
msgstr ""

#: fileItemMenu.js:221
msgid "Extract Here"
msgstr ""

#: fileItemMenu.js:225
msgid "Extract To..."
msgstr ""

#: fileItemMenu.js:232
msgid "Send to..."
msgstr ""

#: fileItemMenu.js:238
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] ""
msgstr[1] ""

#: fileItemMenu.js:244
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] ""
msgstr[1] ""

#: fileItemMenu.js:253
msgid "Common Properties"
msgstr ""

#: fileItemMenu.js:253
msgid "Properties"
msgstr ""

#: fileItemMenu.js:260
msgid "Show All in Files"
msgstr ""

#: fileItemMenu.js:260
msgid "Show in Files"
msgstr ""

#: fileItemMenu.js:336
msgid "Select Extract Destination"
msgstr ""

#: fileItemMenu.js:339
msgid "Select"
msgstr ""

#: fileItemMenu.js:366
msgid "Can not email a Directory"
msgstr ""

#: fileItemMenu.js:367
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: preferences.js:91
msgid "Settings"
msgstr ""

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr ""

#: preferences.js:98
msgid "Tiny"
msgstr ""

#: preferences.js:98
msgid "Small"
msgstr ""

#: preferences.js:98
msgid "Standard"
msgstr ""

#: preferences.js:98
msgid "Large"
msgstr ""

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr ""

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr ""

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr ""

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr ""

#: preferences.js:105
msgid "New icons alignment"
msgstr ""

#: preferences.js:106
msgid "Top-left corner"
msgstr ""

#: preferences.js:107
msgid "Top-right corner"
msgstr ""

#: preferences.js:108
msgid "Bottom-left corner"
msgstr ""

#: preferences.js:109
msgid "Bottom-right corner"
msgstr ""

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr ""

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr ""

#: preferences.js:122
msgid "Click type for open files"
msgstr ""

#: preferences.js:122
msgid "Single click"
msgstr ""

#: preferences.js:122
msgid "Double click"
msgstr ""

#: preferences.js:123
msgid "Show hidden files"
msgstr ""

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr ""

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: preferences.js:130
msgid "Display the content of the file"
msgstr ""

#: preferences.js:131
msgid "Launch the file"
msgstr ""

#: preferences.js:132
msgid "Ask what to do"
msgstr ""

#: preferences.js:138
msgid "Show image thumbnails"
msgstr ""

#: preferences.js:139
msgid "Never"
msgstr ""

#: preferences.js:140
msgid "Local files only"
msgstr ""

#: preferences.js:141
msgid "Always"
msgstr ""

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""

#: showErrorPopup.js:37
msgid "Close"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""
